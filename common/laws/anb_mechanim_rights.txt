﻿law_sapience_mechanim_unrecognized = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	can_enact  = {
		NOT = {
			has_variable = mechanim_wrongs_enforced
		}
	}
	
	is_visible = {
		NOT = {
			has_variable = mechanim_wrongs_enforced
		}
	}

	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_capitalists_pol_str_mult = 0.15
	}
	
	possible_political_movements = {
		law_mechanim_freedom
		law_mechanim_banned
		law_sapience_mechanim_compromise
	}

	ai_will_do = {
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_sapience_mechanim_unrecognized_enforced = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	can_enact  = {
		has_variable = mechanim_wrongs_enforced
	}
	
	is_visible = {
		has_variable = mechanim_wrongs_enforced
	}

	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_capitalists_pol_str_mult = 0.10
		state_radicals_from_sol_change_mult= 0.1
	}
	
	possible_political_movements = {
		law_mechanim_freedom
		law_mechanim_banned
		law_sapience_mechanim_compromise_enforced
	}

	ai_will_do = {
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_sapience_mechanim_compromise = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	can_enact  = {
		NOT = {
			has_variable = mechanim_wrongs_enforced
		}
	}
	is_visible = {
		NOT = {
			has_variable = mechanim_wrongs_enforced
		}
	}
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_capitalists_pol_str_mult = 0.10
	}
	
	possible_political_movements = {
		law_mechanim_freedom
		law_mechanim_banned
		law_sapience_mechanim_unrecognized
	}

	ai_will_do = {
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_sapience_mechanim_compromise_enforced = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	can_enact  = {
		has_variable = mechanim_wrongs_enforced
	}
	
	is_visible = {
		has_variable = mechanim_wrongs_enforced
	}

	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_capitalists_pol_str_mult = 0.05
		state_radicals_from_sol_change_mult= 0.05

	}
	
	possible_political_movements = {
		law_mechanim_freedom
		law_mechanim_banned
		law_sapience_mechanim_unrecognized_enforced
	}

	ai_will_do = {
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

#add the enforced version of laws here

law_mechanim_banned = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
		
	modifier = {
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		custom_tooltip = mechanim_banned_tt
		custom_tooltip = {
			text = mechanim_banned_destroy_tt
			every_scope_state = {
				if = {
					limit = {
						has_building = building_automatories
					}
					remove_building = building_automatories
				}
			}
		}
	}
	
	possible_political_movements = {
		law_sapience_mechanim_unrecognized_enforced
		law_sapience_mechanim_compromise_enforced
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}


law_mechanim_freedom = {
	group = lawgroup_mechanim_rights
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
		

	on_enact = {
		recalculate_pop_ig_support = yes #expand this later when you have time
		set_variable = {
			name = mechanim_wrongs_enforced
			value = flag
		}
		trigger_event = mechanim_revolt.5
		if = {
			limit = {
				NOT = {
					has_journal_entry = je_mechanim_1
				}
			}
			add_journal_entry = { type = je_mechanim_2 }
		}
	}
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}