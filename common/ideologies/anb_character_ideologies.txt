﻿# TODO - look at the modifiers in these files, despite base chances being extremely low there's still a tonne of racial purists etc... about

# this ideology is throwing a bunch of errors and is outdated so i'm commenting
# ideology_racial_purist = {
# 	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
# 	character_ideology = yes

	
# 	# Don't want monsters migrating
# 	lawgroup_migration = {
# 		law_migration_controls = strongly_approve
# 		law_closed_borders = approve
# 		law_no_migration_controls = disapprove	
# 	}
	
# 	lawgroup_racial_tolerance = {
# 		law_same_heritage_only = strongly_approve
# 		law_local_tolerance = strongly_disapprove
# 		law_expanded_tolerance = strongly_disapprove
# 		law_all_heritage = strongly_disapprove	
# 	}

# 	# TODO
# 	non_interest_group_leader_trigger = {
# 		# Agitators should have an ideology at odds with current laws
# 		NAND = {
# 			has_role = agitator
# 			owner = {
# 				OR = {
# 					has_law = law_type:law_same_race_only
# 					has_law = law_type:law_giantkin_group_only
# 				}
# 			}
# 		}
# 	}
	
# 	interest_group_leader_weight = {
# 		value = 10 # Less likely by default
# 		# More likely with a convenient scapegoat and is angry without same race only
# 		if = {
# 			limit = {
# 				owner = {
# 					NOT = { has_law = law_type:law_same_race_only }
# 					any_scope_state = {
# 						is_incorporated = yes
# 						any_scope_pop = {
# 							NOT = { 
# 								pop_race_accepted = {
# 									COUNTRY = owner
# 								}
# 							}
# 						}
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= -5
# 				}
# 			}
# 			add = 25
# 		}
# 		# More likely if angry and issues are relevant
# 		if = {
# 			limit = {
# 				owner = {
# 					NOR = {
# 						has_law = law_type:law_same_race_only
# 						has_law = law_type:law_giantkin_group_only
# 						has_law = law_type:law_ruinborn_group_only
# 						has_law = law_type:law_goblinoid_group_only
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= -5
# 				}
# 			}
# 			add = 50
# 		}
# 		# Less likely in council republic
# 		if = {
# 			limit = {
# 				owner = {
# 					has_law = law_type:law_council_republic
# 				}
# 			}
# 			add = -25
# 		}
# 		# less likely if content without same race only
# 		if = {
# 			limit = {
# 				owner = {
# 					NOT = {
# 						has_law = law_type:law_same_race_only
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= 5
# 				}
# 			}
# 			add = -25
# 		}if = {
# 			limit = {
# 				OR = {
# 					has_variable = communist_ig_var
# 					has_variable = chose_vanguardism
# 					has_variable = chose_anarchism
# 					has_variable = chose_communism
# 					has_variable = chose_ethno_nationalism
# 					has_variable = chose_fascism
# 				}
# 			}
# 			multiply = 0.1
# 		}
# 	}
# 	non_interest_group_leader_weight = {
# 		value = 10
# 		# More likely with a convenient scapegoat and is angry without same race only
# 		if = {
# 			limit = {
# 				owner = {
# 					NOT = { has_law = law_type:law_same_race_only }
# 					any_scope_state = {
# 						is_incorporated = yes
# 						any_scope_pop = {
# 							NOT = { 
# 								pop_race_accepted = {
# 									COUNTRY = owner
# 								}
# 							}
# 						}
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= -5
# 				}
# 			}
# 			add = 25
# 		}
# 		# More likely if angry and issues are relevant
# 		if = {
# 			limit = {
# 				owner = {
# 					NOR = {
# 						has_law = law_type:law_same_race_only
# 						has_law = law_type:law_giantkin_group_only
# 						has_law = law_type:law_ruinborn_group_only
# 						has_law = law_type:law_goblinoid_group_only
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= -5
# 				}
# 			}
# 			add = 50
# 		}
# 		# Less likely in council republic
# 		if = {
# 			limit = {
# 				owner = {
# 					has_law = law_type:law_council_republic
# 				}
# 			}
# 			add = -25
# 		}
# 		# less likely if content without same race only
# 		if = {
# 			limit = {
# 				owner = {
# 					NOT = {
# 						has_law = law_type:law_same_race_only
# 					}
# 				}
# 				scope:interest_group = {
# 					ig_approval <= 5
# 				}
# 			}
# 			add = -25
# 		}
# 		if = {
# 			limit = {
# 				OR = {
# 					has_variable = communist_ig_var
# 					has_variable = chose_vanguardism
# 					has_variable = chose_anarchism
# 					has_variable = chose_communism
# 					has_variable = chose_ethno_nationalism
# 					has_variable = chose_fascism
# 				}
# 			}
# 			multiply = 0.1
# 		}
# 	}
# }

# Wants EVIL artifice
ideology_mad_scientist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
	character_ideology = yes
	
	lawgroup_magic_and_artifice = {
		law_artifice_banned = strongly_disapprove
		law_nation_of_magic = neutral
		law_nation_of_artifice = approve
		law_traditional_magic_banned = approve
	}
	
	interest_group_leader_trigger = {
		owner = {
			NOR = {
				has_law = law_type:law_artifice_banned
				has_law = law_type:law_traditional_magic_banned
			}
		}
	}
	
	non_interest_group_leader_trigger = {
		# Agitators should have an ideology at odds with current laws
		owner = {
			NOR = {
				has_law = law_type:law_artifice_banned
				has_law = law_type:law_traditional_magic_banned
			}
		}
		NAND = {
			has_role = agitator
			owner = {
				has_law = law_type:law_amoral_artifice_embraced
			}
		}
	}
	
	non_interest_group_leader_weight = {
		value = 25
		# Less likely if from a "traditional" IG
		if = {
			limit = {
				OR = {
					is_interest_group_type = ig_landowners
					is_interest_group_type = ig_devout
				}
			}
			add = -75
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}
	}
	
	interest_group_leader_weight = {
		value = 25
		# Less likely if from a "traditional" IG
		if = {
			limit = {
				OR = {
					is_interest_group_type = ig_landowners
					is_interest_group_type = ig_devout
				}
			}
			add = -75
		}
		# more likely when issues are relevant
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_amoral_artifice_banned
						has_law = law_type:law_pragmatic_artifice
					}
				}
			}
			add = 50
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}
	}
}

ideology_magocrat = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_theocrat.dds"
	
	character_ideology = yes

	lawgroup_governance_principles = {
		law_magocracy = strongly_approve	
		law_stratocracy = disapprove
		law_monarchy = neutral
		law_theocracy = disapprove	
		law_presidential_republic = disapprove
		law_parliamentary_republic = disapprove
		law_council_republic = disapprove
	}
	
	lawgroup_magic_and_artifice = {
		law_artifice_banned = strongly_approve
		law_nation_of_magic = approve
		law_nation_of_artifice = disapprove
		law_traditional_magic_banned = strongly_disapprove
	}
	
	interest_group_leader_trigger = {
		OR = {
			owner = {
				has_law = law_type:law_magocracy
			}
			scope:interest_group = {
				is_interest_group_type = ig_landowners
			}
		}
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_petty_bourgeoisie
			is_interest_group_type = ig_landowners
			is_interest_group_type = ig_intelligentsia
		}
	}
	
	non_interest_group_leader_trigger = {
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_petty_bourgeoisie
			is_interest_group_type = ig_landowners
			is_interest_group_type = ig_intelligentsia
		}
		# Agitators should oppose current laws
		NAND = {
			has_role = agitator
			owner = {
				has_law = law_type:law_magocracy
			}
		}
	}
	
	interest_group_leader_weight = {
		value = 100
		# less likely in a theocracy
		if = {
			limit = {
				owner = {
					has_law = law_type:law_theocracy
				}
			}
			add = -75
		}
		# less likely if IG is happy without magocray
		if = {
			limit = {
				owner = {
					NOT = { has_law = law_type:law_magocracy }
				}
				scope:interest_group = {
					ig_approval > 0
				}
			}
			add = -100
		}
		# more likely if IG is happy in a magocracy
		if = {
			limit = {
				owner = {
					has_law = law_type:law_magocracy
				}
				scope:interest_group = {
					ig_approval > 5
				}
			}
			add = 50
		}
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
	
	non_interest_group_leader_weight = {
		value = 100
		# less likely in a theocracy
		if = {
			limit = {
				owner = {
					has_law = law_type:law_theocracy
				}
			}
			add = -75
		}
		# # less likely if IG is happy without magocray
		# if = {
		# 	limit = {
		# 		owner = {
		# 			NOT = { has_law = law_type:law_magocracy }
		# 		}
				
		# 		ig_approval > 0
		# 	}
		# 	add = -100
		# }
		# # more likely if IG is happy in a magocracy
		# if = {
		# 	limit = {
		# 		owner = {
		# 			has_law = law_type:law_magocracy
		# 		}
		# 		scope:interest_group = {
		# 			ig_approval > 5
		# 		}
		# 	}
		# 	add = 50
		# }
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
}

ideology_aldanist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_corporatist.dds"
	
	character_ideology = yes

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = neutral
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = strongly_disapprove
	}
	
	lawgroup_racial_tolerance = {
		law_same_heritage_only = approve
		law_local_tolerance = disapprove
		law_expanded_tolerance = strongly_disapprove
		law_all_heritage = strongly_disapprove	
	}
	
	interest_group_leader_trigger = {
		culture = { has_discrimination_trait = aelantiri_heritage }
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_petty_bourgeoisie
			is_interest_group_type = ig_landowners
			is_interest_group_type = ig_intelligentsia
		}
	}
	
	non_interest_group_leader_trigger = {
		culture = { has_discrimination_trait = aelantiri_heritage }
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_petty_bourgeoisie
			is_interest_group_type = ig_landowners
			is_interest_group_type = ig_intelligentsia
		}
	}
	
	interest_group_leader_weight = {
		value = 50
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
	non_interest_group_leader_weight = {
		value = 50
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
}

ideology_venaanist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_communist.dds"
	
	character_ideology = yes

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = neutral
		law_cultural_exclusion = approve
		law_multicultural = disapprove
	}
	lawgroup_racial_tolerance = {
		law_same_heritage_only = disapprove
		law_local_tolerance = strongly_approve
		law_expanded_tolerance = neutral
		law_all_heritage = strongly_disapprove	
	}
	
	interest_group_leader_trigger = {
		OR = {
			culture = { has_discrimination_trait = aelantiri_heritage }
			culture = { has_discrimination_trait = elven_race_heritage }
		}
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_intelligentsia
			is_interest_group_type = ig_trade_unions
		}
	}
	non_interest_group_leader_trigger = {
		OR = {
			culture = { has_discrimination_trait = aelantiri_heritage }
			culture = { has_discrimination_trait = elven_race_heritage }
		}
		OR = {
			is_interest_group_type = ig_armed_forces
			is_interest_group_type = ig_intelligentsia
			is_interest_group_type = ig_trade_unions
		}
	}
	
	interest_group_leader_weight = {
		value = 50
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
	
	non_interest_group_leader_weight = {
		value = 50
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
}


ideology_traditionalist_equal = {
 	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_traditionalist.dds"
	
 	character_ideology = yes
	
 	lawgroup_rights_of_women = {
 		law_womens_suffrage = disapprove
 		law_women_in_the_workplace = disapprove
 		law_women_own_property = neutral
 		law_no_womens_rights = strongly_disapprove
 	}

 	lawgroup_church_and_state = {
 		law_state_religion = approve
 		law_freedom_of_conscience = disapprove
 		law_total_separation = strongly_disapprove
 		law_state_atheism = strongly_disapprove
 	}
	
 	interest_group_leader_trigger = {
 		owner = { has_technology_researched = tradition_of_equality }
 		NOT = { owner = { any_primary_culture = { has_discrimination_trait = kheionai } } }
 		ideology_traditionalist_valid_trigger = yes
 	}
 	non_interest_group_leader_trigger = {
 		owner = { has_technology_researched = tradition_of_equality }
 		NOT = { culture = { has_discrimination_trait = kheionai } }
 		interest_group = {
 			ideology_traditionalist_valid_trigger = yes
 		}
 		NAND = {
 			has_role = agitator
 			owner = {
 				has_law = law_type:law_womens_suffrage
 				has_law = law_type:law_state_religion
 			}
 		}
 	}
	
 	interest_group_leader_weight = {
 		value = 100
 		if = {
 			limit = {
 				OR = {
 					has_variable = communist_ig_var
 					has_variable = chose_vanguardism
 					has_variable = chose_anarchism
 					has_variable = chose_communism
 					has_variable = chose_ethno_nationalism
 					has_variable = chose_fascism
 				}
 			}
 			multiply = 0.1
 		}	
 	}
	
 	non_interest_group_leader_weight = {
 		value = 100
 		if = {
 			limit = {
 				interest_group = {
 					OR = {
 						has_variable = communist_ig_var
 						has_variable = chose_vanguardism
 						has_variable = chose_anarchism
 						has_variable = chose_communism
 						has_variable = chose_ethno_nationalism
 						has_variable = chose_fascism
 					}
 				}
 			}
 			multiply = 0.1
 		}	
 	}
}


ideology_mechanim_liberator = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_abolitionist.dds"
	
	character_ideology = yes
	
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_approve
		law_mechanim_banned = strongly_disapprove
		law_sapience_mechanim_compromise_enforced = neutral
		law_sapience_mechanim_compromise = neutral
		law_sapience_mechanim_unrecognized_enforced = strongly_disapprove
		law_sapience_mechanim_unrecognized = strongly_disapprove
	}
	
	interest_group_leader_trigger = {
		AND = {
			OR = {
				is_interest_group_type = ig_intelligentsia
				is_interest_group_type = ig_trade_unions
				is_interest_group_type = ig_rural_folk
				is_interest_group_type = ig_petty_bourgeoisie
				is_interest_group_type = ig_industrialists
			}
			owner = { has_technology_researched = human_rights }
		}
	}
	non_interest_group_leader_trigger = {
		NAND = {
			has_role = agitator
			owner = {
				has_law = law_type:law_mechanim_freedom
			}
		}
		AND = {
			OR = {
				is_interest_group_type = ig_intelligentsia
				is_interest_group_type = ig_trade_unions
				is_interest_group_type = ig_rural_folk
				is_interest_group_type = ig_petty_bourgeoisie
				is_interest_group_type = ig_industrialists
			}
			owner = { has_technology_researched = human_rights }
		}
	}

	interest_group_leader_weight = {
		value = 145
		if = {
			limit = { is_interest_group_type = ig_trade_unions }
			add = 150
		}	
		if = {
			limit = { is_interest_group_type = ig_intelligentsia }
			add = 75	
		}		
		if = {
			limit = { has_trait = tactful }
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = human_rights }
			}
			add = 50
		}
		if = {
			limit = { is_interest_group_type = ig_industrialists }
			subtract = 50	
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}	
	}
	
	non_interest_group_leader_weight = {
		value = 145
		if = {
			limit = { scope:interest_group = { is_interest_group_type = ig_trade_unions } }
			add = 150
		}	
		if = {
			limit = { scope:interest_group = { is_interest_group_type = ig_intelligentsia } }
			add = 75	
		}		
		if = {
			limit = { has_trait = tactful }
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = human_rights }
			}
			add = 50
		}
		if = {
			limit = { scope:interest_group = { is_interest_group_type = ig_industrialists } }
			subtract = 50	
		}
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}	
	}
}


ideology_mechanim_slaver= {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_slaver.dds"
	
	character_ideology = yes
	
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_disapprove
		law_mechanim_banned = strongly_disapprove
		law_sapience_mechanim_compromise_enforced = approve
		law_sapience_mechanim_compromise = approve
		law_sapience_mechanim_unrecognized_enforced = strongly_approve
		law_sapience_mechanim_unrecognized = strongly_approve
	}
	
	interest_group_leader_trigger = {
		AND = {
			OR = {
				is_interest_group_type = ig_industrialists
				is_interest_group_type = ig_landowners
				is_interest_group_type = ig_rural_folk
				is_interest_group_type = ig_petty_bourgeoisie
			}
		}
	}
	non_interest_group_leader_trigger = {
		NAND = {
			has_role = agitator
			owner = {
				has_law = law_type:law_mechanim_freedom
			}
		}
		AND = {
			interest_group = {
				OR = {
					is_interest_group_type = ig_industrialists
					is_interest_group_type = ig_landowners
					is_interest_group_type = ig_rural_folk
					is_interest_group_type = ig_petty_bourgeoisie
				}
			}
		}
	}


	interest_group_leader_weight = {
		value = 115
		if = {
			limit = {
				owner = {
					has_law = law_type:law_mechanim_freedom
				}
			}
			add = -350
		}	
		if = {
			limit = { is_interest_group_type = ig_industrialists }
			add = 95	
		}
		if = {
			limit = { is_interest_group_type = ig_rural_folk }
			add = 35	
		}							
		if = {
			limit = {
				owner = { has_technology_researched = human_rights }
			}
			add = -50
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}	
	}
	non_interest_group_leader_weight = {
		value = 115
		if = {
			limit = {
				owner = {
					has_law = law_type:law_mechanim_freedom
				}
			}
			add = -350
		}	
		if = {
			limit = { is_interest_group_type = ig_industrialists }
			add = 95	
		}
		if = {
			limit = { is_interest_group_type = ig_rural_folk }
			add = 35	
		}							
		if = {
			limit = {
				owner = { has_technology_researched = human_rights }
			}
			add = -50
		}
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}	
	}
}

ideology_eordand_unification_leader = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_sovereignist.dds"
	character_ideology = yes

	lawgroup_church_and_state = {
		law_state_religion = neutral
		law_freedom_of_conscience = strongly_approve
		law_total_separation = disapprove
		law_state_atheism = strongly_disapprove
	}
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = neutral
	}
	
	lawgroup_migration = {
		law_closed_borders = strongly_disapprove
		law_migration_controls = approve
		law_no_migration_controls = neutral
	}
	
	interest_group_leader_trigger = {
		owner = {
			any_primary_culture = { has_discrimination_trait = eordan }
		}
		NOT = { exists = c:B87 }
	}
	non_interest_group_leader_trigger = {
		OR ={
			culture = cu:selphereg
			culture = cu:caamas
			culture = cu:peitar
			culture = cu:tuathak
			culture = cu:snecboth
			culture = cu:fograc
		}
		NOT = { exists = c:B87 }
	}


	interest_group_leader_weight = {
		value = 10
		if = {
			limit = {
				owner = {
					NOT = { has_law = law_type:law_freedom_of_conscience }
				}
			}
			add = 75
		}
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_national_supremacy
						has_law = law_type:law_ethnostate
					}
				}
			}
			add = 50
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_closed_borders
				}
			}
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = nationalism }
			}
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = pan-nationalism }
			}
			add = 200
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}
	}
	non_interest_group_leader_weight = {
		value = 10
		if = {
			limit = {
				owner = {
					NOT = { has_law = law_type:law_freedom_of_conscience }
				}
			}
			add = 100
		}
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_national_supremacy
						has_law = law_type:law_ethnostate
					}
				}
			}
			add = 50
		}
		if = {
			limit = {
				owner = {
					has_law = law_type:law_closed_borders
				}
			}
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = nationalism }
			}
			add = 50
		}
		if = {
			limit = {
				owner = { has_technology_researched = pan-nationalism }
			}
			add = 200
		}
		if = {
			limit = {
				interest_group = {
					OR = {
						has_variable = communist_ig_var
						has_variable = chose_vanguardism
						has_variable = chose_anarchism
						has_variable = chose_communism
						has_variable = chose_ethno_nationalism
						has_variable = chose_fascism
					}
				}
			}
			multiply = 0.1
		}
	}
}
ideology_marshalist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/jackson_democrat.dds" 
	
	character_ideology = yes

	lawgroup_governance_principles = {
		law_magocracy = neutral	
		law_stratocracy = strongly_approve
		law_monarchy = neutral
		law_theocracy = strongly_disapprove	
		law_presidential_republic = neutral
		law_parliamentary_republic = disapprove
		law_council_republic = disapprove
	}
	
	lawgroup_distribution_of_power = {
		law_autocracy = strongly_approve
		law_technocracy = approve
		law_oligarchy = neutral
		law_landed_voting = neutral	
		law_wealth_voting = disapprove	
		law_census_voting = disapprove		
		law_universal_suffrage = disapprove
		law_single_party_state = approve
		law_anarchy = strongly_disapprove
	}
	
	lawgroup_army_model = {
		law_peasant_levies = disapprove
		law_professional_army = approve
		law_national_militia = strongly_disapprove
		law_mass_conscription = approve
	}

	lawgroup_internal_security = {
		law_no_home_affairs = disapprove
		law_national_guard = strongly_approve
		law_secret_police = approve
		law_guaranteed_liberties = disapprove
	}

	lawgroup_policing = {
		law_no_police = disapprove
		law_local_police = neutral
		law_dedicated_police = approve
		law_militarized_police = strongly_approve
	}
	
	interest_group_leader_trigger = {
		OR = {
			OR = {
				is_interest_group_type = ig_armed_forces
				is_interest_group_type = ig_landowners
				is_interest_group_type = ig_petty_bourgeoisie
			}
			religion = rel:godlost
		}
	}
	non_interest_group_leader_trigger = {
		# Agitators should oppose current laws
		NAND = {
			has_role = agitator
			owner = {
				has_law = law_type:law_stratocracy
			}
		}
		OR = {
			interest_group = {
				OR = {
					is_interest_group_type = ig_armed_forces
					is_interest_group_type = ig_landowners
					is_interest_group_type = ig_petty_bourgeoisie
				}
			}
			religion = rel:godlost
		}
	}
	
	non_interest_group_leader_weight = {
		value = 10 # Less likely by default
		# More likely when already a stratocracy
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_stratocracy
					}
				}
			}
			add = 100
		}
		# Less likely if from a "Progressive" IG
		if = {
			limit = {
				OR = {
					is_interest_group_type = ig_intelligentsia
					is_interest_group_type = ig_trade_unions
				}
			}
			multiply = { value = 0.5 }
		}
		# More likely if from a "Militaristic" IG
		if = {
			limit = {
				is_interest_group_type = ig_armed_forces
			}
			add = 50
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}
	}
	interest_group_leader_weight = {
		value = 10 # Less likely by default
		# More likely when already a stratocracy
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_stratocracy
					}
				}
			}
			add = 100
		}
		# Less likely if from a "Progressive" IG
		if = {
			limit = {
				OR = {
					is_interest_group_type = ig_intelligentsia
					is_interest_group_type = ig_trade_unions
				}
			}
			multiply = { value = 0.5 }
		}
		# More likely if from a "Militaristic" IG
		if = {
			limit = {
				is_interest_group_type = ig_armed_forces
			}
			add = 50
		}
		if = {
			limit = {
				OR = {
					has_variable = communist_ig_var
					has_variable = chose_vanguardism
					has_variable = chose_anarchism
					has_variable = chose_communism
					has_variable = chose_ethno_nationalism
					has_variable = chose_fascism
				}
			}
			multiply = 0.1
		}
	}
}