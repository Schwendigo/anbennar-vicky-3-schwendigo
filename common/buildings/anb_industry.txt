﻿building_magical_reagents_workshop = {
	building_group = bg_magical_reagents_workshop
	icon = "gfx/interface/icons/building_icons/magical_reagents_workshop.dds"
	city_type = city
	levels_per_mesh = 5
	
	unlocking_technologies = {
		fractional_distillation
	}

	production_method_groups = {
		pmg_method_magical_reagents_workshop
        pmg_blueblood_magical_reagents_workshop
		pmg_automation_magical_reagents_workshop
	}

	required_construction = construction_cost_high
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_light_industry.dds"
}

building_doodad_manufacturies = {
	building_group = bg_doodad_manufacturies
	icon = "gfx/interface/icons/building_icons/doodad_manufacturies.dds"
	city_type = city
	levels_per_mesh = 5

	ai_nationalization_desire = -0.25 # Just to use this new modifier, lets say that making doodads cos its both consumer good and usage good is pretty much led by private sector

	unlocking_technologies = {
		punch_card_artificery
	}

	production_method_groups = {
		pmg_power_supply_building_doodad_manufacturies
		pmg_shell_building_doodad_manufacturies
		pmg_stabilization_building_doodad_manufacturies
		pmg_automation_building_doodad_manufacturies
	}

	required_construction = construction_cost_very_high

	possible = {
		owner = {
			NOT = { has_law = law_type:law_industry_banned }
		}
	}

	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_heavy_industry.dds"
}

building_automatories = {
	building_group = bg_automatories
	icon = "gfx/interface/icons/building_icons/automata_manufacturies.dds"
	city_type = city
	levels_per_mesh = 5

	ai_nationalization_desire = 0.25 # AI is a bit reluctant to privatize government goods	#put this here as I assume gob will want to keep an eye on automata

	unlocking_technologies = {
		insyaan_analytical_engine
	}

	production_method_groups = {
		pmg_base_production_building_automatories
		pmg_control_method_building_automatories
		pmg_automation_building_automatories
	}

	required_construction = construction_cost_very_high

	possible = {
		owner = {
			NOT = { has_law = law_type:law_mechanim_banned }
		}
	}

	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_heavy_industry.dds"
}