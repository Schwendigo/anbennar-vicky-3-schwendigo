﻿# AI = {
# 	# Non-hostile goals for all starting subjects
# 	every_country = {
# 		save_scope_as = first_country
# 		every_country = {
# 			limit = { is_subject_of = prev }
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}		
# 	}
	
# 	# German Confederation + Switzerland - Great Powers
# 	every_country = {
# 		limit = {
# 			country_rank = rank_value:great_power
# 			any_country = {			
# 				OR = {
# 					country_has_primary_culture = cu:north_german
# 					country_has_primary_culture = cu:south_german
# 					country_has_primary_culture = cu:swiss
# 				}				
# 			}
# 		}
			
# 		save_scope_as = first_country
# 		every_country = {
# 			limit = { 
# 				NOT = { country_rank = rank_value:great_power }
# 				OR = {
# 					country_has_primary_culture = cu:north_german
# 					country_has_primary_culture = cu:south_german
# 					country_has_primary_culture = cu:swiss
# 				}				
# 			}
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}
# 	}	
	
# 	# German Confederation + Switzerland - everyone else
# 	every_country = {
# 		limit = {
# 			NOT = { country_rank = rank_value:great_power }
# 			any_country = {			
# 				OR = {
# 					country_has_primary_culture = cu:north_german
# 					country_has_primary_culture = cu:south_german
# 					country_has_primary_culture = cu:swiss
# 				}				
# 			}
# 		}
			
# 		save_scope_as = first_country
# 		every_country = {
# 			limit = { 
# 				NOT = { country_rank = rank_value:great_power }
# 				OR = {
# 					country_has_primary_culture = cu:north_german
# 					country_has_primary_culture = cu:south_german
# 					country_has_primary_culture = cu:swiss
# 				}				
# 			}
# 			effect_set_limited_non_hostile_mutual_secret_goal = yes
# 		}
# 	}		
	
# 	# Despicably neutral krakow
# 	c:KRA = {
# 		set_mutual_secret_goal = {
# 			country = c:AUS
# 			secret_goal = none
# 		}
# 		set_mutual_secret_goal = {
# 			country = c:PRU
# 			secret_goal = none
# 		}
# 		set_mutual_secret_goal = {
# 			country = c:RUS
# 			secret_goal = none
# 		}		
# 	}
	
# 	# United States of America
# 	c:USA = {
# 		set_secret_goal = {
# 			country = c:TEX
# 			secret_goal = protect
# 		}
# 		set_mutual_secret_goal = {
# 			country = c:MEX
# 			secret_goal = antagonize
# 		}	
# 		set_mutual_secret_goal = {
# 			country = c:HAI
# 			secret_goal = none
# 		}
# 		set_mutual_secret_goal = {
# 			country = c:HBC
# 			secret_goal = none
# 		}	
# 		set_mutual_secret_goal = {
# 			country = c:ORG
# 			secret_goal = none
# 		}
# 		set_mutual_secret_goal = {
# 			country = c:QUE
# 			secret_goal = none
# 		}	
# 		set_mutual_secret_goal = {
# 			country = c:ONT
# 			secret_goal = none
# 		}			
# 	}	
	
# 	# Texas
# 	c:TEX = {
# 		set_secret_goal = {
# 			country = c:USA
# 			secret_goal = befriend
# 		}
# 	}		

# 	# China
# 	c:CHI = {
# 		set_secret_goal = {
# 			country = c:GBR
# 			secret_goal = none
# 		}
# 	}

# 	# Great Britain
# 	c:GBR = {
# 		set_secret_goal = {
# 			country = c:BEL
# 			secret_goal = protect
# 		}	
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = protect
# 		}		
# 		set_secret_goal = {
# 			country = c:FRA
# 			secret_goal = antagonize
# 		}
# 		set_secret_goal = {
# 			country = c:CHI
# 			secret_goal = none # Will be set by the opium wars
# 		}
		
# 		save_scope_as = first_country
# 		c:GRE = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}			
# 	}
	
# 	# France
# 	c:FRA = {
# 		set_secret_goal = {
# 			country = c:GBR
# 			secret_goal = antagonize
# 		}
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = protect
# 		}		

# 		save_scope_as = first_country
# 		c:GRE = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}		
# 		c:SAR = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}	
# 		c:SWI = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}			
# 	}

# 	# Sweden
# 	c:SWE = {
# 		save_scope_as = first_country
# 		c:FIN = {
# 			effect_set_non_hostile_mutual_secret_goal = yes
# 		}		
# 	}		

# 	# Prussia
# 	c:PRU = {
# 		set_secret_goal = {
# 			country = c:AUS
# 			secret_goal = antagonize
# 		}
		
# 		save_scope_as = first_country
# 		c:SWE = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}		
# 	}
	
# 	# Austria
# 	c:AUS = {
# 		set_secret_goal = {
# 			country = c:PRU
# 			secret_goal = antagonize
# 		}			
# 	}	

# 	# Netherlands
# 	c:NET = {
# 		random_list = {
# 			80 = {
# 				set_secret_goal = {
# 					country = c:BEL
# 					secret_goal = antagonize
# 				}				
# 			}
# 			20 = {
# 				set_secret_goal = {
# 					country = c:BEL
# 					secret_goal = conquer
# 				}				
# 			}
# 		}
# 	}
	
# 	# Belgium
# 	c:BEL = {
# 		set_secret_goal = {
# 			country = c:NET
# 			secret_goal = antagonize
# 		}
# 		set_secret_goal = {
# 			country = c:GBR
# 			secret_goal = befriend
# 		}		
# 	}	

# 	# Russia
# 	c:RUS = {
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = conquer
# 		}	

# 		save_scope_as = first_country
# 		c:GRE = {
# 			effect_set_non_hostile_mutual_secret_goal_power_difference = yes
# 		}		
# 	}
	
	
# 	# Brazil
# 	c:BRZ = {
# 		set_secret_goal = {
# 			country = c:PNI
# 			secret_goal = conquer
# 		}
# 		set_secret_goal = {
# 			country = c:PRA
# 			secret_goal = conquer
# 		}		
# 	}	
	
# 	# Piratini
# 	c:PNI = {
# 		set_secret_goal = {
# 			country = c:BRZ
# 			secret_goal = antagonize
# 		}	
# 		set_secret_goal = {
# 			country = c:URU
# 			secret_goal = befriend
# 		}		
# 	}	
	
# 	# Grao-Para
# 	c:PRA = {
# 		set_secret_goal = {
# 			country = c:BRZ
# 			secret_goal = antagonize
# 		}		
# 	}	
	
# 	# Ottomans
# 	c:TUR = {
# 		set_secret_goal = {
# 			country = c:RUS
# 			secret_goal = conquer
# 		}
# 		set_secret_goal = {
# 			country = c:EGY
# 			secret_goal = conquer
# 		}		
# 	}

# 	# Siam
# 	c:SIA = {
# 		set_secret_goal = {
# 			country = c:DAI
# 			secret_goal = antagonize
# 		}		
# 	}
	
# 	# Dai Nam
# 	c:DAI = {
# 		set_secret_goal = {
# 			country = c:SIA
# 			secret_goal = antagonize
# 		}		
# 	}

# 	# Egypt
# 	c:EGY = {
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = conquer
# 		}		
# 	}
	
# 	# Wallachia
# 	c:WAL = {
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = antagonize
# 		}		
# 	}	
	
# 	# Moldavia
# 	c:MOL = {
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = antagonize
# 		}		
# 	}
	
# 	# Serbia
# 	c:SER = {
# 		set_secret_goal = {
# 			country = c:TUR
# 			secret_goal = antagonize
# 		}		
# 	}	
	
# 	# Tibet
# 	c:TIB = {
# 		set_secret_goal = {
# 			country = c:CHI
# 			secret_goal = antagonize
# 		}		
# 	}		
# }	

AI = {
	#Northern League
	c:A04 = {
		set_secret_goal = { #GH
			country = c:A06
			secret_goal = antagonize
		}
		set_mutual_secret_goal = { #Grombar
			country = c:A10
			secret_goal = antagonize
		}
		set_secret_goal = { #Triarchy
			country = c:B07
			secret_goal = befriend
		}
		set_secret_goal = { #Trollsbay
			country = c:B98
			secret_goal = befriend
		}
		set_secret_goal = { #Nortiochand
			country = c:B60
			secret_goal = protect
		}
	}

	#Gnomish Hierarchy
	c:A06 = {
		#insert more global stuff before
		set_mutual_secret_goal = { #Nortiochand
			country = c:B60
			secret_goal = antagonize
		}	
		set_secret_goal = { #Freemarches
			country = c:B53
			secret_goal = protect
		}
		set_secret_goal = { #Dragon Dominion
			country = c:B49
			secret_goal = none
		}	
		set_secret_goal = { #Eordand
			country = c:B82
			secret_goal = antagonize
		}	
		set_secret_goal = { #Eordand
			country = c:B83
			secret_goal = antagonize
		}
		set_secret_goal = { #Eordand
			country = c:B84
			secret_goal = antagonize
		}	
	}

	#Trollsbay Concord
	c:B98 = {
		set_secret_goal = { #Ynnsmouth
			country = c:B21
			secret_goal = protect
		}
		set_mutual_secret_goal = { #Redglades
			country = c:B18
			secret_goal = antagonize
		}	
		set_secret_goal = { #Nur Ionnidar
			country = c:B26
			secret_goal = antagonize
		}
		set_mutual_secret_goal = {
			country = c:B29
			secret_goal = none #Should be dynamically set by events/JE depending on situation
		}	
		set_secret_goal = { #South Expanse
			country = c:B34
			secret_goal = befriend
		}
		set_secret_goal = {
			country = c:B41
			secret_goal = befriend
		}	
		set_secret_goal = {
			country = c:B42
			secret_goal = befriend
		}
		set_secret_goal = { #Triarchy
			country = c:B07
			secret_goal = befriend
		}
	}

	#Sarda Empire
	c:B29 = {
		set_mutual_secret_goal = { #Dragon Dominion
			country = c:B49
			secret_goal = antagonize
		}
		set_secret_goal = { #Ynnsmouth
			country = c:B21
			secret_goal = protect
		}
		set_mutual_secret_goal = { #Veykoda
			country = c:B31
			secret_goal = befriend
		}	
		set_mutual_secret_goal = { #Argezvale
			country = c:B36
			secret_goal = befriend
		}	
		set_secret_goal = { #Bosancovac
			country = c:B39
			secret_goal = protect
		}
		set_secret_goal = { #Minata
			country = c:B46
			secret_goal = protect
		}
		set_secret_goal = { #Vels Fadhecai
			country = c:B45
			secret_goal = protect
		}
	}

	#Dragon Dominion
	c:B29 = {
		set_secret_goal = { #Ynnsmouth
			country = c:B21
			secret_goal = protect
		}
		set_secret_goal = { #Bosancovac
			country = c:B39
			secret_goal = protect
		}	
		set_mutual_secret_goal = { #Amacimst
			country = c:B38
			secret_goal = antagonize
		}	
		set_mutual_secret_goal = { #Drevkenuc
			country = c:B52
			secret_goal = antagonize
		}
		set_mutual_secret_goal = { #Arganjuzorn
			country = c:B47
			secret_goal = befriend
		}
		set_secret_goal = { #Ranger Republic
			country = c:B91
			secret_goal = none
		}
		set_secret_goal = { #Noo Coddoran
			country = c:B62
			secret_goal = antagonize
		}
		set_secret_goal = { #Eordand Admin
			country = c:B80
			secret_goal = none
		}
	}

	#Triarchy
	c:B07 = {
		set_secret_goal = { #Haraf
			country = c:B10
			secret_goal = antagonize
		}
		set_mutual_secret_goal = { #VG
			country = c:B05
			secret_goal = befriend
		}
	}

	#VG
	c:B05 = {
		set_mutual_secret_goal = { #Kheios
			country = c:C30
			secret_goal = antagonize
		}
		set_mutual_secret_goal = { #Eordand
			country = c:B82
			secret_goal = antagonize
		}	
		set_mutual_secret_goal = { #Eordand
			country = c:B83
			secret_goal = antagonize
		}
		set_mutual_secret_goal = { #Eordand
			country = c:B84
			secret_goal = antagonize
		}
	}

	#Argezvale
	c:B36 = {
		set_secret_goal = { #New Havoral
			country = c:B35
			secret_goal = antagonize
		}
	}

	#Amadia
	c:C02 = {
		set_mutual_secret_goal = { #Ozgarom
			country = c:C03
			secret_goal = antagonize
		}
	}

}


