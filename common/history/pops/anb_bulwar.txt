﻿POPS = {
	s:STATE_REUYEL = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 870000
			}
			create_pop = {
				culture = sun_elven
				size = 116000
			}
			create_pop = {
				culture = bahari_goblin
				size = 174000
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 58000
			}
		}
	}
	s:STATE_CRATHANOR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 98280
			}
			create_pop = {
				culture = ilatani
				size = 580720
			}
			create_pop = {
				culture = bahari_goblin
				size = 91000
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}

		}
		region_state:A09 = {
			create_pop = {
				culture = ilatani
				size = 140000
			}
			create_pop = {
				culture = copper_dwarf
				size = 14560
			}
			create_pop = {
				culture = silver_dwarf
				size = 3640
				religion = ravelian
			}
		}
	}
	s:STATE_MEDBAHAR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 276000
			}
			create_pop = {
				culture = sun_elven
				size = 34500
			}
			create_pop = {
				culture = bahari_goblin
				size = 258500
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 172500
			}
			create_pop = {
				culture = marble_dwarf
				religion = ancestor_worship
				size = 86000
			}	
		}
	}
	s:STATE_OVDAL_TUNGR = {
		region_state:F15 = {
			create_pop = {
				culture = bahari
				size = 88635
			}
			create_pop = {
				culture = ilatani
				size = 51704
			}
			create_pop = {
				culture = busilari
				size = 7386
			}
			create_pop = {
				culture = copper_dwarf
				size = 2806773
			}

		}
	}
	s:STATE_MEGAIROUS = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 398400
			}
			create_pop = {
				culture = sun_elven
				size = 207500
			}
			create_pop = {
				culture = bahari_goblin
				size = 207500
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = firanyan_harpy
				size = 16600
			}
			create_pop = {
				culture = marble_dwarf
				religion = ancestor_worship
				size = 78000
			}
		}
	}
	s:STATE_AQATBAHAR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 804000
			}
			create_pop = {
				culture = sun_elven
				size = 100500
			}
			create_pop = {
				culture = firanyan_harpy
				size = 201000
			}
			create_pop = {
				culture = bahari_goblin
				size = 962500
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 72360
			}
			create_pop = {
				culture = marble_dwarf
				size = 8040
			}
			create_pop = {
				culture = imperial_gnome
				size = 40200
			}

		}
	}
	s:STATE_BAHAR_PROPER = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 1092000
			}
			create_pop = {
				culture = sun_elven
				size = 819000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 136500
			}
			create_pop = {
				culture = bahari_goblin
				size = 682500
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 72360
			}

		}
	}
	s:STATE_DROLAS = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 504000
				split_religion = {
					brasanni = {
						the_jadd = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 172800
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 14400
			}
			create_pop = {
				culture = firanyan_harpy
				size = 28800
			}
		}
	}
	s:STATE_ANNAIL = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 303460
			}
			create_pop = {
				culture = sun_elven
				size = 57802
			}
			create_pop = {
				culture = firanyan_harpy
				size = 45416
			}
			create_pop = {
				culture = imperial_gnome
				size = 6193
			}

		}
	}
	s:STATE_KUZARAM = {
		region_state:F09 = {
			create_pop = {
				culture = bahari
				size = 528360
			}
			create_pop = {
				culture = gelkari
				size = 101565
			}
			create_pop = {
				culture = sun_elven
				size = 37000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 74000
			}
			create_pop = {
				culture = black_orc
				size = 32400
				religion = the_jadd
			}

		}
	}
	s:STATE_BRASAN = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 1862327
			}
			create_pop = {
				culture = zanite
				size = 38399
			}
			create_pop = {
				culture = busilari
				size = 19199
			}
			create_pop = {
				culture = sun_elven
				size = 483000
			}
			create_pop = {
				culture = imperial_gnome
				size = 12075
			}
			create_pop = {
				culture = copper_dwarf
				size = 86940
			}
			create_pop = {
				culture = gold_dwarf
				size = 9660
			}

		}
	}
	s:STATE_SAD_SUR = {
		region_state:F01 = {
			create_pop = {
				culture = brasanni
				size = 60000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 578000
			}

		}
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 243600
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 20000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 18400
			}

		}
	}
	s:STATE_LOWER_BURANUN = {
		region_state:F01 = {
			create_pop = {
				culture = brasanni
				size = 1249560
			}
			create_pop = {
				culture = zanite
				size = 112320
			}
			create_pop = {
				culture = surani
				size = 42120
			}
			create_pop = {
				culture = sun_elven
				size = 156000
			}
			create_pop = {
				culture = copper_dwarf
				size = 31200
			}

		}
	}
	s:STATE_LOWER_SURAN = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 1081920
			}
			create_pop = {
				culture = zanite
				size = 45080
			}
			create_pop = {
				culture = sun_elven
				size = 483000
			}
			create_pop = {
				culture = copper_dwarf
				size = 24150
			}

		}
	}
	s:STATE_BULWAR = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 2221110
			}
			create_pop = {
				culture = brasanni
				size = 127650
			}
			create_pop = {
				culture = surani
				size = 204240
			}
			create_pop = {
				culture = sun_elven
				size = 517500
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 172500
			}
			create_pop = {
				culture = firanyan_harpy
				size = 172500
			}
			create_pop = {
				culture = copper_dwarf
				size = 82800
			}
			create_pop = {
				culture = gold_dwarf
				size = 20700
			}
			create_pop = {
				culture = royal_harimari
				size = 34500
				religion = the_jadd
			}

		}
	}
	s:STATE_KUMARKAND = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 1252800
			}
			create_pop = {
				culture = brasanni
				size = 139200
			}
			create_pop = {
				culture = sun_elven
				size = 348000
			}
			create_pop = {
				culture = copper_dwarf
				size = 103500
			}

		}
	}
	s:STATE_WEST_NAZA = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 1765800
			}
			create_pop = {
				culture = sun_elven
				size = 348800
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 65400
			}
			create_pop = {
				culture = copper_dwarf
				size = 39240
			}
			create_pop = {
				culture = gold_dwarf
				size = 4360
			}

		}
	}
	s:STATE_EAST_NAZA = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 1943826
			}
			create_pop = {
				culture = surani
				size = 124074
			}
			create_pop = {
				culture = sun_elven
				size = 847500
			}
			create_pop = {
				culture = firanyan_harpy
				size = 339000
			}
			create_pop = {
				culture = desert_centaur
				size = 135600
				religion = the_jadd
			}
			create_pop = {
				culture = copper_dwarf
				size = 30510
			}
			create_pop = {
				culture = gold_dwarf
				size = 3390
			}

		}
	}
	s:STATE_JADDANZAR = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 1520480
			}
			create_pop = {
				culture = zanite
				size = 194480
			}
			create_pop = {
				culture = brasanni
				size = 35360
			}
			create_pop = {
				culture = masnsih
				size = 17680
			}
			create_pop = {
				culture = sun_elven
				size = 1248000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 416000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 416000
			}
			create_pop = {
				culture = bahari_goblin
				size = 41600
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 83200
			}
			create_pop = {
				culture = gold_dwarf
				size = 66560
			}
			create_pop = {
				culture = peridot_dwarf
				size = 16640
				religion = the_jadd
			}
			create_pop = {
				culture = royal_harimari
				size = 62400
				religion = the_jadd
			}

		}
	}
	s:STATE_AVAMEZAN = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 1180000
			}
			create_pop = {
				culture = surani
				size = 190000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 240000
			}
			create_pop = {
				culture = copper_dwarf
				size = 51520
			}
			create_pop = {
				culture = gold_dwarf
				size = 12880
			}

		}
	}
	s:STATE_UPPER_SURAN = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 450000
			}
			create_pop = {
				culture = sun_elven
				size = 80000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 230000
			}
			create_pop = {
				culture = copper_dwarf
				size = 9120
			}
			create_pop = {
				culture = gold_dwarf
				size = 2280
			}

		}
	}
	s:STATE_AZKA_SUR = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 692550
			}
			create_pop = {
				culture = sun_elven
				size = 324000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 324000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 162000
			}
			create_pop = {
				culture = royal_harimari
				size = 81000
				religion = the_jadd
			}
			create_pop = {
				culture = citrine_dwarf
				size = 87480
			}
			create_pop = {
				culture = gold_dwarf
				size = 58320
			}

		}
	}
	s:STATE_GARLAS_KEL = {
		region_state:F10 = {
			create_pop = {
				culture = bahari
				size = 215208
				religion = old_sun_cult
			}
			create_pop = {
				culture = gelkari
				size = 86000
				religion = old_sun_cult
			}
			create_pop = {
				culture = sun_elven
				size = 193500
			}
			create_pop = {
				culture = firanyan_harpy
				size = 10000
			}

		}
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 40000
			}
			create_pop = {
				culture = sun_elven
				size = 40000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 112000
			}
		}
	}
	s:STATE_HARPYLEN = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 793800
			}
			create_pop = {
				culture = firanyan_harpy
				size = 631800
			}
			create_pop = {
				culture = bahari_goblin
				size = 162000
				split_religion = {
					bahari_goblin = {
						the_jadd = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = copper_dwarf
				size = 48600
			}
			create_pop = {
				culture = black_orc
				size = 32400
				religion = the_jadd
			}

		}
	}
	s:STATE_EAST_HARPY_HILLS = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 333200
			}
			create_pop = {
				culture = firanyan_harpy
				size = 346800
			}
			create_pop = {
				culture = copper_dwarf
				size = 7800
			}
			create_pop = {
				culture = lead_dwarf
				size = 7800
			}

		}
	}
	s:STATE_FIRANYALEN = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 131100
			}
			create_pop = {
				culture = firanyan_harpy
				size = 407100
			}
			create_pop = {
				culture = black_orc
				size = 151800
				religion = the_jadd
			}

		}
	}
	s:STATE_ARDU = {
		region_state:F01 = {
			create_pop = {
				culture = masnsih
				size = 52000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 420000
			}

		}
	}
	s:STATE_KERUHAR = {
		region_state:F01 = {
			create_pop = {
				culture = masnsih
				size = 48450
			}
			create_pop = {
				culture = sun_elven
				size = 68000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 51000
			}
			create_pop = {
				culture = gold_dwarf
				size = 2040
			}
			create_pop = {
				culture = peridot_dwarf
				size = 510
			}

		}
	}
	s:STATE_FAR_EAST_SALAHAD = {
		region_state:F01 = {
			create_pop = {
				culture = masnsih
				size = 20000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}

		}
	}
	s:STATE_EBBUSUBTU = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 5980
			}
			create_pop = {
				culture = raghamidesh
				size = 41860
			}
			create_pop = {
				culture = masnsih
				size = 11960
			}
			create_pop = {
				culture = sun_elven
				size = 46000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 27600
			}
			create_pop = {
				culture = siadunan_harpy
				size = 80500
			}
			create_pop = {
				culture = gold_dwarf
				size = 6900
			}
			create_pop = {
				culture = peridot_dwarf
				size = 2300
			}
			create_pop = {
				culture = copper_dwarf
				size = 2300
			}
			create_pop = {
				culture = royal_harimari
				size = 9200
				religion = the_jadd
			}
		}
	}
	s:STATE_MULEN = {
		region_state:F01 = {
			create_pop = {
				culture = masnsih
				size = 134750
			}
			create_pop = {
				culture = raghamidesh
				size = 40250
			}
			create_pop = {
				culture = sun_elven
				size = 35000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 448000
			}
			create_pop = {
				culture = gold_dwarf
				size = 20000
			}
			create_pop = {
				culture = desert_centaur
				size = 7000
				religion = the_jadd
			}
		}
		region_state:D25 = {
			create_pop = {
				culture = gold_dwarf
				size = 50000
			}
			create_pop = {
				culture = masnsih
				size = 17000
			}
		}
	}
	s:STATE_ELAYENNA = {
		region_state:F01 = {
			create_pop = {
				culture = masnsih
				size = 79560
			}
			create_pop = {
				culture = raghamidesh
				size = 42840
			}
			create_pop = {
				culture = sun_elven
				size = 149600
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 13600
			}
			create_pop = {
				culture = siadunan_harpy
				size = 394400
			}
			create_pop = {
				culture = peridot_dwarf
				size = 10880
			}
			create_pop = {
				culture = gold_dwarf
				size = 2720
			}

		}
	}
}