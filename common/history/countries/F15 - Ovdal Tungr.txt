﻿COUNTRIES = {
	c:F15 ?= {
		effect_starting_technology_tier_3_tech = yes
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_magic

		#consumption taxes
		add_taxed_goods = g:services
		add_taxed_goods = g:liquor
	}
}