﻿COUNTRIES = {
	c:C02 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		
		activate_law = law_type:law_nation_of_artifice
		
		set_variable = {
			name = slavery_recently_abolished
			value = yes
			days = 3650
		}
		every_scope_state = {
			limit = {
				any_scope_pop = {
					has_pop_culture = green_orc
				}
			}
			set_variable = {
				name = former_slave_state
				value = yes
				days = 3650
			}
		}
	}
}