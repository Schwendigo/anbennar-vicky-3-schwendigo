﻿COUNTRIES = {
	c:F04 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_no_womens_rights
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned
	}
}