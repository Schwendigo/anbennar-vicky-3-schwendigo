﻿CHARACTERS = {
	c:R19 ?= {
		create_character = {
			first_name = "Behyakun"
			last_name = "Dragonborn"
			historical = yes
			ruler = yes
			female = yes
			age = 40
			culture = cu:dragon_hobgoblin
			interest_group = ig_armed_forces
			ideology = ideology_jingoist_leader
			traits = {
				imperious
				ambitious
			}
		}
	}
}
