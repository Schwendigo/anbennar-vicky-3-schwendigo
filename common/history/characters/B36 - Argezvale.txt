﻿CHARACTERS = {
	c:B36 ?= {
		create_character = { #
			first_name = Brok 
			last_name = Runebeard
			historical = yes
			ruler = yes
			age = 91
			interest_group = ig_industrialists
			ideology = ideology_protectionist
			traits = {
				engineer tactful pious
			}
		}
	}
}
