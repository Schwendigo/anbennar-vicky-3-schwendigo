﻿CHARACTERS = {
	c:B26 ?= {
		create_character = {
			first_name = Tiren
			last_name = Iascirn
			historical = yes
			ruler = yes
			age = 34
			culture = cheshoshi
			religion = regent_court
			interest_group = ig_landowners
			ig_leader = yes
			is_general = yes
			ideology = ideology_jingoist_leader
			traits = {
				reckless surveyor
			}
		}
	}
}
