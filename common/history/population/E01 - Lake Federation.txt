﻿POPULATION = {
	c:E01 = { #Kalsyto (copied from Japan)
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:E02 = { #Magharma
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:E03 = { #Zabutodask
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:E04 = { #Shivusdoyen
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:E05 = { #River Centaur
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:E06 = { #West Desert Centaur
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:E07 = { #East Desert Centaur
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:E08 = { #West Steppe Centaur
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:E09 = { #East Steppe Centaur
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
}