﻿DIPLOMACY = {
    c:C34 = { #Eneion
		create_diplomatic_pact = { 
			country = c:C35 #Besolaki
			type = trade_agreement
		}
	}
	c:A06 = { #Hierarchy
		create_diplomatic_pact = { 
			country = c:A14 #Small Country
			type = foreign_investment_rights
		}
	}

	c:B29 = { #Sarda Empire
		create_diplomatic_pact = { #Very close ties to isolate New Havoral
			country = c:B36 #Argezvale
			type = trade_agreement
		}
	}

	c:B07 = { #Triarchy
		create_diplomatic_pact = { #Artificer economies are integrated
			country = c:B05 #VG
			type = trade_agreement
		}
	}
	c:A01 = { #Anbennar
		create_diplomatic_pact = { 
			country = c:D08 #Khugdihr
			type = foreign_investment_rights
		}
	}
	c:D08 = { #Khugdihr
		create_diplomatic_pact = { 
			country = c:A10 #Grombar
			type = trade_agreement
		}
	}

	c:D25 = { #Verkal Gulan
		create_diplomatic_pact = { 
			country = c:R02 #Ghankedhen
			type = foreign_investment_rights
		}
	}

	c:L34 = { #Adzalan
		create_diplomatic_pact = {
			country = c:L31 #Thunder Wielding Buffalo
			type = trade_agreement
		}	
	}
}