﻿je_yansheni_unification_idea = {
	icon = "gfx/interface/icons/event_icons/waving_flag.dds"
	
	group = je_group_historical_content

	is_shown_when_inactive = {
		OR = {
			country_has_primary_culture = cu:jiangszun
			country_has_primary_culture = cu:beikling
			country_has_primary_culture = cu:gangim
			country_has_primary_culture = cu:naamjyut
			country_has_primary_culture = cu:jiangyang
		}
	}

	possible = {
		OR = {
			country_has_primary_culture = cu:jiangszun
			country_has_primary_culture = cu:beikling
			country_has_primary_culture = cu:gangim
			country_has_primary_culture = cu:naamjyut
			country_has_primary_culture = cu:jiangyang
		}
		has_technology_researched = nationalism
	}
 
	complete = {		
		any_country = {
			filter = {
				OR = {
					country_has_primary_culture = cu:jiangszun
					country_has_primary_culture = cu:beikling
					country_has_primary_culture = cu:gangim
					country_has_primary_culture = cu:naamjyut
					country_has_primary_culture = cu:jiangyang				
				}
			}
			has_technology_researched = nationalism	
			percent >= 0.75
		}
	}

	on_complete = {
		hidden_effect = {
			if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:great_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:great_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}
			else_if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:great_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang					
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:great_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}
			else_if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:major_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}						
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:major_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}	
			else_if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:major_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang					
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:major_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}
			else_if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:unrecognized_major_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}						
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:unrecognized_major_power 
						OR = {
							AND = {
								country_has_primary_culture = cu:jiangszun
								root = { country_has_primary_culture = cu:jiangszun }
							}						
							AND = {
								country_has_primary_culture = cu:beikling
								root = { country_has_primary_culture = cu:beikling }
							}
							AND = {
								country_has_primary_culture = cu:gangim
								root = { country_has_primary_culture = cu:gangim }
							}
							AND = {
								country_has_primary_culture = cu:naamjyut
								root = { country_has_primary_culture = cu:naamjyut }
							}
							AND = {
								country_has_primary_culture = cu:jiangyang
								root = { country_has_primary_culture = cu:jiangyang }
							}						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}	
			else_if = {
				limit = {
					any_country = { 
						country_rank >= rank_value:unrecognized_major_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang					
						}
					}			
				}			
				random_country = {
					limit = { 
						country_rank >= rank_value:unrecognized_major_power 
						OR = {
							country_has_primary_culture = cu:jiangszun
							country_has_primary_culture = cu:beikling
							country_has_primary_culture = cu:gangim
							country_has_primary_culture = cu:naamjyut
							country_has_primary_culture = cu:jiangyang						
						}					
					}
					save_scope_as = yanshen_unifier_candidate
				}				
			}			
		}	
		
		hidden_effect = { set_variable = je_german_unification_idea }
		trigger_event = { id = german_unification.5 } # German National Identity
	}

	status_desc = {
		first_valid = {
			triggered_desc = { 
				desc = je_german_unification_idea_status
			}
		}
	}

	weight = 1000
	should_be_pinned_by_default = yes
}

je_yansheni_unification = {
	icon = "gfx/interface/icons/event_icons/waving_flag.dds"
	
	group = je_group_historical_content

	is_shown_when_inactive = {
		OR = {
			country_has_primary_culture = cu:jiangszun
			country_has_primary_culture = cu:beikling
			country_has_primary_culture = cu:gangim
			country_has_primary_culture = cu:naamjyut
			country_has_primary_culture = cu:jiangyang						
		}		
	}

	possible = {
		OR = {
			country_rank >= rank_value:major_power
			country_rank >= rank_value:unrecognized_major_power
		}
		is_subject = no
	}

	complete = {
		custom_tooltip = {
			c:Y02 ?= this
			text = je_german_unification_complete_trigger_desc
		}	
	}

	on_complete = {
		show_as_tooltip = {
			trigger_event = { id = german_unification.4 popup = yes } # German Unification
		}
		remove_variable = je_unification_progress
	}

	status_desc = {
		first_valid = {
			triggered_desc = {
				desc = je_german_status
			}
		}
	}

	invalid = {
		country_has_primary_culture = cu:hungarian
	}

	weight = 1000
	should_be_pinned_by_default = yes
}