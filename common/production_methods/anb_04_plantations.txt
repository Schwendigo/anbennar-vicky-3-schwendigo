﻿## Coffee Plantation

pm_growth_beans_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods																
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_coffee_add = 10
		}
	}
}

high_velocity_irrigation_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_coffee_add = 18	
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}
chronoponics_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_coffee_add = 36	
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Cotton Plantation


pm_growth_beans_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4		
			
			# output goods													
			goods_output_fabric_add = 20
		}

	}
}
high_velocity_irrigation_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_fabric_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}
chronoponics_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_fabric_add = 80
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}
## Dye Plantation
pm_growth_beans_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_dye_add = 12
		}

	}
}

high_velocity_irrigation_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_dye_add = 22
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}
chronoponics_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_dye_add = 44
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Opium Plantation
pm_growth_beans_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_opium_add = 12
		}

	}
}
high_velocity_irrigation_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_opium_add = 22
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}
chronoponics_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_opium_add = 44
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Tea Plantation
pm_growth_beans_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_tea_add = 10
		}

	}
}

high_velocity_irrigation_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_tea_add = 18
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}

chronoponics_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_tea_add = 36
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}
## Tobacco Plantation
pm_growth_beans_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_tobacco_add = 12
		}

	}
}

high_velocity_irrigation_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_tobacco_add = 22
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}

chronoponics_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_tobacco_add = 44
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Sugar Plantation
pm_growth_beans_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_sugar_add = 15
		}

	}
}

high_velocity_irrigation_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_sugar_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}

chronoponics_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_sugar_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Banana Plantation
pm_growth_beans_banana_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_fruit_add = 15
		}

	}
}

high_velocity_irrigation_banana_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_fruit_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}

pmg_enhancements_banana_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_fruit_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Silk Plantation
pm_growth_beans_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_silk_add = 10
		}

	}
}

high_velocity_irrigation_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_silk_add = 18
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}

chronoponics_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_silk_add = 36
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

## Vineyard Plantation
pm_growth_beans_vineyard_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		intensive_agriculture			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
					
			goods_input_artificery_doodads_add = 4				
			
			# output goods													
			goods_output_wine_add = 10	#todo REBALANCE
		}

	}
}

high_velocity_irrigation_vineyard_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		gene_transmutation		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 9
			
			# output goods
			goods_output_wine_add = 18
		}

		level_scaled = {
			building_employment_laborers_add = -250
			
			building_employment_machinists_add = 250
		}
	}
}


chronoponics_vineyard_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			
			goods_input_artificery_doodads_add = 15
			
			# output goods
			goods_output_wine_add = 36
		}

		level_scaled = {
			building_employment_laborers_add = -500
			
			building_employment_machinists_add = 500
		}
	}
}

