﻿gov_dakocracy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_DAK"
	female_ruler = "RULER_DAK"
	
	possible = {
		c:D31 = ROOT
		has_law = law_type:law_magocracy
		country_has_voting_franchise = no
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_absolute_magocracy = {
	transfer_of_power = dictatorial
	new_leader_on_reform_government = no

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = no
		NOR = {
			has_law = law_type:law_oligarchy
			has_law = law_type:law_technocracy
		}
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_magocratic_oligarchy = {
	transfer_of_power = dictatorial
	new_leader_on_reform_government = no

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		OR = {
			has_law = law_type:law_oligarchy
			has_law = law_type:law_technocracy
		}
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_magocratic_democracy = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = yes
	}

	on_government_type_change = {
		change_to_presidential_elective = yes
	}
	on_post_government_type_change = {
		post_change_to_presidential_elective = yes
	}
}
